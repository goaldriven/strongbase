<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

defined( 'ABSPATH' ) or die( '' );

add_action( 'carbon_fields_register_fields', 'sb_theme_options' );

function sb_theme_options() {
	$root = Container::make( 'theme_options', __( 'StrongBase Theme Settings' ) )
	                 ->set_page_menu_title( 'StrongBase' )
	                 ->set_icon( 'dashicons-admin-appearance' )
	                 ->add_fields( array() );

	Container::make( 'theme_options', __( 'Developer' ) )
	         ->set_page_parent( $root )
	         ->add_fields( array(
		         Field::make( 'checkbox', 'goaldriven_wordpress-supports', __( 'Use Gd Wordpress Supports' ) )
		              ->set_option_value( 'yes' ),
		         Field::make( 'checkbox', 'loco-translate', __( 'Use Loco Translation' ) )
		              ->set_option_value( 'yes' )
	         ) );
}