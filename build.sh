#!/usr/bin/env bash

THEME_SLUG=strongbase

rm -rf build/$THEME_SLUG
mkdir -p build/$THEME_SLUG

rsync -avz --exclude '.git' --exclude 'build' . build/$THEME_SLUG/

cd build/
zip -r $THEME_SLUG.zip $THEME_SLUG