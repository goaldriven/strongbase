<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitd765123592ce7b3175a2beec0433d5b5
{
    public static $files = array (
        '7b11c4dc42b3b3023073cb14e519683c' => __DIR__ . '/..' . '/ralouphie/getallheaders/src/getallheaders.php',
        'a0edc8309cc5e1d60e3047b5df6b7052' => __DIR__ . '/..' . '/guzzlehttp/psr7/src/functions_include.php',
        '0e6d7bf4a5811bfa5cf40c5ccd6fae6a' => __DIR__ . '/..' . '/symfony/polyfill-mbstring/bootstrap.php',
        '8b0d424e6572b6620fcae6008af510ef' => __DIR__ . '/..' . '/htmlburger/wpemerge/config.php',
        '270cb6edcaac5923bab5c49ecf5fbd06' => __DIR__ . '/..' . '/htmlburger/wpemerge/src/functions.php',
        '3336c9935fd0994a30e8c7cc51ee93a7' => __DIR__ . '/..' . '/htmlburger/wpemerge/src/load.php',
        '1967dd89f918dd1e702a2eb2a5cc4f22' => __DIR__ . '/..' . '/htmlburger/carbon-pagination/carbon-pagination.php',
        '8c9ae19209e937f99f287ae041fb5d0c' => __DIR__ . '/..' . '/htmlburger/wpemerge-cli/config.php',
        '19e8616eb39c638d5398d219fc54fc75' => __DIR__ . '/..' . '/htmlburger/wpemerge-theme-core/config.php',
        'f441812896742131a9f0ef4fecc5b8c9' => __DIR__ . '/..' . '/htmlburger/wpemerge-theme-core/src/load.php',
    );

    public static $prefixLengthsPsr4 = array (
        'W' => 
        array (
            'Whoops\\' => 7,
            'WPEmerge\\Cli\\' => 13,
            'WPEmerge\\' => 9,
            'WPEmergeTheme\\' => 14,
            'WPEmergeTestTools\\' => 18,
        ),
        'S' => 
        array (
            'Symfony\\Polyfill\\Mbstring\\' => 26,
            'Symfony\\Component\\Process\\' => 26,
            'Symfony\\Component\\Debug\\' => 24,
            'Symfony\\Component\\Console\\' => 26,
        ),
        'P' => 
        array (
            'Psr\\Log\\' => 8,
            'Psr\\Http\\Message\\' => 17,
            'Psr\\Container\\' => 14,
        ),
        'G' => 
        array (
            'GuzzleHttp\\Psr7\\' => 16,
        ),
        'C' => 
        array (
            'Composer\\Installers\\' => 20,
            'Carbon_Fields\\' => 14,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Whoops\\' => 
        array (
            0 => __DIR__ . '/..' . '/filp/whoops/src/Whoops',
        ),
        'WPEmerge\\Cli\\' => 
        array (
            0 => __DIR__ . '/..' . '/htmlburger/wpemerge-cli/src',
        ),
        'WPEmerge\\' => 
        array (
            0 => __DIR__ . '/..' . '/htmlburger/wpemerge/src',
        ),
        'WPEmergeTheme\\' => 
        array (
            0 => __DIR__ . '/..' . '/htmlburger/wpemerge-theme-core/src',
        ),
        'WPEmergeTestTools\\' => 
        array (
            0 => __DIR__ . '/..' . '/htmlburger/wpemerge/tests/tools',
        ),
        'Symfony\\Polyfill\\Mbstring\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-mbstring',
        ),
        'Symfony\\Component\\Process\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/process',
        ),
        'Symfony\\Component\\Debug\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/debug',
        ),
        'Symfony\\Component\\Console\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/console',
        ),
        'Psr\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/log/Psr/Log',
        ),
        'Psr\\Http\\Message\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/http-message/src',
        ),
        'Psr\\Container\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/container/src',
        ),
        'GuzzleHttp\\Psr7\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/psr7/src',
        ),
        'Composer\\Installers\\' => 
        array (
            0 => __DIR__ . '/..' . '/composer/installers/src/Composer/Installers',
        ),
        'Carbon_Fields\\' => 
        array (
            0 => __DIR__ . '/..' . '/htmlburger/carbon-fields/core',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app/src',
        ),
    );

    public static $prefixesPsr0 = array (
        'P' => 
        array (
            'Pimple' => 
            array (
                0 => __DIR__ . '/..' . '/pimple/pimple/src',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitd765123592ce7b3175a2beec0433d5b5::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitd765123592ce7b3175a2beec0433d5b5::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInitd765123592ce7b3175a2beec0433d5b5::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
